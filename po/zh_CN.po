# Chinese translations for spellutils package
# spellutils 软件包的简体中文翻译.
# Copyright (C) 2022 Free Software Foundation, Inc.
# This file is distributed under the same license as the spellutils package.
# Boyuan Yang <byang@debian.org>, 2022.
#
msgid ""
msgstr ""
"Project-Id-Version: spellutils 0.7+debian\n"
"Report-Msgid-Bugs-To: https://tracker.debian.org/pkg/spellutils\n"
"POT-Creation-Date: 2022-12-02 13:41-0500\n"
"PO-Revision-Date: 2022-12-02 13:42-0500\n"
"Last-Translator: Boyuan Yang <073plan@gmail.com>\n"
"Language-Team: Chinese (simplified) <i18n-zh@googlegroups.com>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.2\n"

#: pospell.c:162
msgid "only one -l option is allowed"
msgstr "只允许一个 -l 选项"

#: pospell.c:185 newsbody.c:192
#, c-format
msgid "getopt returned impossible value: %d ('%c')"
msgstr "getopt 返回不可能出现的值：%d ('%c')"

#: pospell.c:206
#, c-format
msgid "Usage: pospell [-fsv] [-l language] -n pofile -p program\n"
"       [-- [program-arguments...]]\n"
"\n"
"Copies all translated strings from `pofile' to a temporary\n"
"file (the `spellfile') and then calls the program `program'\n"
"(typically a spell checker) with all `program-arguments'.\n"
"Afterwards the possibly changed translation is copied back\n"
"into the `pofile'.\n"
"\n"
"If `pofile' is specified as `-', pospell will use its standard\n"
"input and standard output.\n"
"\n"
"`%f' in the `program arguments' will be expanded to the name of\n"
"the `spellfile'. Escape single percent sign with two characters\n"
"(%%).\n"
"\n"
"The flags mean:\n"
" -f: The called program is a filter so don't make a temporary\n"
"     file, but pipe `spellfile' to the standard input of the\n"
"     `program' and later read it back from its standard output.\n"
" -l language: Only translations with the indicated language code\n"
"     are copied to the `spellfile'.\n"
" -s: quit with an error message if the `pofile' or `spellfile'\n"
"     have unrecognized formats.\n"
" -v: Print version and exit.\n"
msgstr "用法：pospell [-fsv] [-l 语言] -n po文件 -p 程序\n"
"      [-- [程序参数...]]\n"
"\n"
"将所有来自指定“po文件”的已翻译的字符串复制到临时文件\n"
"（“spell文件”）中，然后调用指定的“程序”（通常为拼写\n"
"检查工具）并使用给出的“程序参数”。\n"
"在此之后，将可能被修改的翻译复制回“po文件”中。\n"
"\n"
"如果指定的“po文件”是“-”，那么 pospell 将会使用其标准\n"
"输入和标准输出。\n"
"\n"
"在“程序参数”中出现的“%f”将扩展成“spell文件”的\n"
"名称。请将单个百分号符号改用两个符号进行转义（%%）。\n"
"\n"
"参数的含义：\n"
" -f: 所调用的程序是一个过滤器因而不创建临时文件，而是采取将\n"
"     “spell文件”通过管道输入给定“程序”的标准输入，并稍后\n"
"     从其对应标准输出读回内容的工作方式。\n"
" -l 语言: 只将带有指定的语言代码的翻译复制到“spell文件”中。\n"
" -s: 如果“po文件”或“spell文件”有无法识别的格式则提示错误\n"
"     消息并退出。\n"
" -v: 打印版本号并退出。\n"

#: pospell.c:284 newsbody.c:294
#, c-format
msgid "%s died with signal \"%s\""
msgstr ""

#: pospell.c:287 newsbody.c:297
#, c-format
msgid "%s died with signal no. %d"
msgstr ""

#: pospell.c:294 newsbody.c:304
#, c-format
msgid "child process couldn't execute %s"
msgstr "子进程无法执行 %s"

#: pospell.c:296 newsbody.c:306
#, c-format
msgid "%s returned failure exit code %d"
msgstr "%s 返回失败退出代码 %d"

#: pospell.c:299 newsbody.c:309
#, c-format
msgid "impossible status from child process: %x"
msgstr "来自子进程的不可能的状态：%x"

#: pospell_write.c:141
#, c-format
msgid "I don't understand this line in the .po file:\n"
"%s"
msgstr "我不理解 .po 文件中的这一行：\n"
"%s"

#: pospell_read.c:67
msgid "found marker for a removed item in the spell file,\n"
"but cannot find a corresponding item in the po file"
msgstr ""

#: pospell_read.c:124
#, c-format
msgid "found line with unknown content in the spell file:\n"
"%s"
msgstr "在 spell 文件中找到含有未知内容的行：\n"
"%s"

#: newsbody.c:212
#, c-format
msgid "Usage: newsbody [-fhqsv] [-k headername] -n message -p program\n"
"       [-- [program-arguments...]]\n"
"\n"
"Copies the body of the e-mail or usenet message in `message'\n"
"to a temporary file (the `bodyfile') and then calls the program\n"
"`program' (e.g. a spell checker) with all `program-arguments'.\n"
"Afterwards the possibly changed `bodyfile' is copied back into\n"
"the `message'.\n"
"\n"
"If `message' is specified as `-', newsbody will use its\n"
"standard input and standard output.\n"
"\n"
"`%f' in the `program arguments' will be expanded to the name of\n"
"the `bodyfile'. Use `%%' for a real `%' character.\n"
"\n"
"The flags mean:\n"
" -f: The called program is a filter so don't make a temporary\n"
"     file, but pipe `bodyfile' to the standard input of the\n"
"     `program' and later read it back from its standard output.\n"
" -h: keep the entire header. This flag should also be used\n"
"     if 'message' doesn't have a header at all.\n"
" -k headername: keep these header lines in the `bodyfile'.\n"
"     (Multiple -k flags are allowed).\n"
" -q: Also remove quotes from the `bodyfile'.\n"
"     Lines starting with `>' are considered quotes.\n"
" -s: Also remove signature from the `bodyfile'.\n"
"     The signature is all lines from the first signature separator\n"
"     (i.e. `-- ') to the bottom of the message.\n"
" -v: Print version and exit.\n"
msgstr ""

#: newsbody_read.c:109
msgid "found marker for a removed line in the body file,\n"
"but cannot find a corresponding line in the message file"
msgstr ""

#: header.c:53
msgid "internal error while reading a header continuation"
msgstr ""

#: lib.c:37 lib.c:47
msgid "could not allocate memory"
msgstr "无法分配内存"

#: lib.c:70
msgid "reading file"
msgstr ""

#: lib.c:95
msgid "writing file"
msgstr ""

#: lib.c:103
#, c-format
msgid "open %s for reading"
msgstr ""

#: lib.c:112
#, c-format
msgid "open %s for writing"
msgstr ""

#: lib.c:119
msgid "closing file"
msgstr ""

#: lib.c:128 lib.c:135
msgid "making temporary filename"
msgstr ""

#: lib.c:144
#, c-format
msgid "unlink %s"
msgstr ""

#: lib.c:152
#, c-format
msgid "rename %s %s"
msgstr ""

#: lib.c:165
#, c-format
msgid "stat %s"
msgstr ""

#: lib.c:174
#, c-format
msgid "read from %s"
msgstr ""

#: lib.c:176
#, c-format
msgid "write to %s"
msgstr ""

#: lib.c:204
msgid "error in child process"
msgstr "子进程中出现错误"

#: lib.c:204
msgid "error"
msgstr "错误"

#: lib.c:215
#, c-format
msgid ", errno = %d"
msgstr ""

#: exec.c:38 exec.c:93
msgid "fork"
msgstr ""

#: exec.c:54
msgid "open terminal read"
msgstr ""

#: exec.c:56 exec.c:106
msgid "dup2 stdin"
msgstr ""

#: exec.c:58
msgid "close extra terminal read fd"
msgstr ""

#: exec.c:63
msgid "open terminal write"
msgstr ""

#: exec.c:65 exec.c:113
msgid "dup2 stdout"
msgstr "dup2 标准输出"

#: exec.c:67
msgid "close extra terminal write fd"
msgstr ""

#: exec.c:70 exec.c:118
#, c-format
msgid "exec %s"
msgstr ""

#: exec.c:87
msgid "pipe for output"
msgstr ""

#: exec.c:89
msgid "pipe for input"
msgstr ""

#: exec.c:104
msgid "close write end of output pipe"
msgstr ""

#: exec.c:108 exec.c:123
msgid "close read end of output pipe"
msgstr ""

#: exec.c:111
msgid "close read end of input pipe"
msgstr ""

#: exec.c:115 exec.c:128
msgid "close write end of input pipe"
msgstr ""

#: exec.c:125
msgid "fdopen write end of output pipe"
msgstr ""

#: exec.c:130
msgid "fdopen read end of input pipe"
msgstr ""

#: exec.c:138
msgid "waiting for child"
msgstr "正在等待子进程"

